package v1alpha1

import (
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "k8s.io/apimachinery/pkg/runtime"
  "k8s.io/apimachinery/pkg/runtime/schema"
)

var SchemeGroupVersion = schema.GroupVersion{
  Group:   "xmarlem.dev",
  Version: "v1alpha1",
}

var (
  SchemeBuilder runtime.SchemeBuilder
  AddToScheme   = SchemeBuilder.AddToScheme
)

func init() {

  // devo registrare i miei nuovi tipi in kubernetes... lo faccio tramite SchemeBuilder, che prende una funzione come parametro
  SchemeBuilder.Register(addKnownTypes)
}

// la funzione da passare al Register di schemebuilder
func addKnownTypes(scheme *runtime.Scheme) error {
  // this instruction registers the new types to the scheme
  scheme.AddKnownTypes(SchemeGroupVersion, &Kluster{}, &KlusterList{})

  //
  metav1.AddToGroupVersion(scheme, SchemeGroupVersion)
  return nil
}
