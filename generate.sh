#!/bin/bash -e

CURRENT_DIR=$(pwd)
GEN_DIR=$(dirname $0)
REPO_DIR="$CURRENT_DIR/$GEN_DIR/../.."

PROJECT_MODULE="gitlab.com/xmarlem1/k8s/kluster"
IMAGE_NAME="kubernetes-codegen:latest"

CUSTOM_RESOURCE_NAME="kluster"
CUSTOM_RESOURCE_VERSION="v1alpha1"

# echo "Building codegen Docker image..."
# nerdctl build -f "${GEN_DIR}/Dockerfile" \
#              -t "${IMAGE_NAME}" \
#              "${REPO_DIR}"

cmd="./generate-groups.sh all \
    "$PROJECT_MODULE/pkg/client" \
    "$PROJECT_MODULE/pkg/apis" \
    $CUSTOM_RESOURCE_NAME:$CUSTOM_RESOURCE_VERSION \
    --go-header-file ./hack/boilerplate.go.txt"

echo "Generating client codes..."
nerdctl run --rm \
           -v "${REPO_DIR}:/go/src/${PROJECT_MODULE}" \
           "${IMAGE_NAME}" $cmd

echo $REPO_DIR
chown $USER:$USER -R $REPO_DIR/pkg
