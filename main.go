package main

import (
  "context"
  "flag"
  "fmt"
  "log"
  "path/filepath"

  kclientset "gitlab.com/xmarlem1/k8s/kluster/pkg/generated/clientset/versioned"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "k8s.io/client-go/tools/clientcmd"
  "k8s.io/client-go/util/homedir"
)

func main() {

  var kubeconfig *string

  if home := homedir.HomeDir(); home != "" {
    kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
  } else {
    kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
  }
  flag.Parse()

  config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
  if err != nil {
    log.Printf("Building config from flags, %s", err.Error())
  }

  // clientset, err := kubernetes.NewForConfig(config)
  // if err != nil {
  //   log.Printf("Building kubeclient %s\n", err.Error())
  // }

  klientset, err := kclientset.NewForConfig(config)
  if err != nil {
    log.Printf("Building kubeclient %s\n", err.Error())
  }

  klusters, err := klientset.XmarlemV1alpha1().Klusters("default").List(context.Background(), metav1.ListOptions{})
  if err != nil {
    log.Printf("listing klusters %s\n", err.Error())

  }
  fmt.Printf("length of klusters is %d\n", len(klusters.Items))

}
