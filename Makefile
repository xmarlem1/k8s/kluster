M=

.PHONY: git
git:
	git add . && git commit -m "${M}" && git push -u origin HEAD

.PHONY: generate
generate:
	./hack/update-codegen.sh

.PHONY: manifests
manifests:
	controller-gen paths=gitlab.com/xmarlem1/k8s/kluster/pkg/apis/xmarlem.dev/v1alpha1 crd:crdVersions=v1 output:crd:artifacts:config=manifests

.PHONY: install
install:
	kubectl apply -f manifests/xmarlem.dev_klusters.yaml


.PHONY: uninstall
uninstall:
	kubectl delete -f manifests/xmarlem.dev_klusters.yaml


.PHONY: clear
clear:
	rm -rf pkg/apis/xmarlem.dev/v1alpha1/zz_* pkg/generated manifests


.PHONY: run
run:
	@echo "Build..."
	@go build -o kluster .
	@echo "Run..."
	@./kluster
